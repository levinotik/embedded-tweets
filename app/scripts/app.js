'use strict';

var app = angular.module('embeddedTweetsAppApp', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute'
]);
app.config(function($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'views/main.html',
            controller: 'MainCtrl'
        })
        .otherwise({
            redirectTo: '/'
        });
});

app.filter('unsafe', function($sce) {
    return function(val) {
        return $sce.trustAsHtml(val);
    };
});