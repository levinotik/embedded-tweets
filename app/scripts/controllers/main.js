'use strict';

app.controller('MainCtrl', function($scope, $timeout) {
    $scope.tweets = [{
        embedHtml: "<blockquote class=\"twitter-tweet tw-align-center\"><p>Search API will now always return \"real\" Twitter user IDs. The with_twitter_user_id parameter is no longer necessary. An era has ended. ^TS</p>&mdash; Twitter API (@twitterapi) <a href=\"https://twitter.com/twitterapi/status/133640144317198338\" data-datetime=\"2011-11-07T20:21:07+00:00\">November7, 2011</a></blockquote>\n<script src=\"http://platform.twitter.com/widgets.js\" charset=\"utf-8\"></script>"
    }];


    $scope.onTimeout = function() {
        $scope.tweets.push({
            embedHtml: "<blockquote class=\"twitter-tweet tw-align-center\"><p>Search API will now always return \"real\" Twitter user IDs. The with_twitter_user_id parameter is no longer necessary. An era has ended. ^TS</p>&mdash; Twitter API (@twitterapi) <a href=\"https://twitter.com/twitterapi/status/133640144317198338\" data-datetime=\"2011-11-07T20:21:07+00:00\">November7, 2011</a></blockquote>"
        })
        $timeout($scope.onTimeout, 5000);
    }

    $timeout($scope.onTimeout, 2000);

});